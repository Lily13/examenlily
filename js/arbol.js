class Arbol{
    constructor(){
        this.NodoPadre = this.CrearPadre();
        this.Buscar;
    }

    CrearPadre(padre = null, nivel = null, valor = "A"){
        var nodo = new Nodo(padre, nivel, valor);
        return nodo;
    }
    
    CrearNodo(padre, nivel, valor){
        var nodo = new Nodo(padre, nivel, valor);
        return nodo;
    }

    BusquedaNivel(Nodo, nivel, buscar){
    
        if(Nodo.nivel == nivel)
            buscar.push(Nodo.valor);
    
        if(Nodo.hasOwnProperty('Ni'))
            this.BusquedaNivel(Nodo.Ni, nivel, buscar);
        
        if(Nodo.hasOwnProperty('Nd'))
            this.BusquedaNivel(Nodo.Nd, nivel, buscar);


        return buscar;
    }

    BusquedaElemento(Nodo, elemento){

        if(Nodo.hasOwnProperty('Ni'))
            this.BusquedaElemento(Nodo.Ni, elemento);
        
        if(Nodo.hasOwnProperty('Nd'))
            this.BusquedaElemento(Nodo.Nd, elemento);
    
        if(Nodo.valor == elemento){
            this.Buscar = Nodo;
        }
    
        return this.Buscar;
    }

    CaminoNodo(Nodo){

        if(Nodo.padre != null){
            Camino = Camino + ' ' + Nodo.padre.valor;
            this.CaminoNodo(Nodo.padre);
        }
    
        return Nodo.valor + Camino;
    
    }    
    
}

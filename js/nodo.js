class Nodo{
    constructor(padre, nivel, valor){
        this.valor = valor;
        this.padre = padre;

        if(nivel == null)
            this.nivel = 0;

        if(nivel != null)
            this.nivel = nivel + 1;
    }
}
var arbol = new Arbol(); //Instanciar 

arbol.NodoPadre.Ni = arbol.CrearNodo(arbol.NodoPadre, arbol.NodoPadre.nivel, "B");
arbol.NodoPadre.Ni.Ni = arbol.CrearNodo(arbol.NodoPadre.Ni, arbol.NodoPadre.Ni.nivel, "D");
arbol.NodoPadre.Ni.Ni.Ni = arbol.CrearNodo(arbol.NodoPadre.Ni.Ni, arbol.NodoPadre.Ni.Ni.nivel, "G");
arbol.NodoPadre.Ni.Ni.Ni.Nd = arbol.CrearNodo(arbol.NodoPadre.Ni.Ni.Ni, arbol.NodoPadre.Ni.Ni.Ni.nivel, "k");

arbol.NodoPadre.Nd = arbol.CrearNodo(arbol.NodoPadre, arbol.NodoPadre.nivel, "C");
arbol.NodoPadre.Nd.Ni = arbol.CrearNodo(arbol.NodoPadre.Nd, arbol.NodoPadre.Nd.nivel, "E");
arbol.NodoPadre.Nd.Ni.Ni = arbol.CrearNodo(arbol.NodoPadre.Nd.Ni, arbol.NodoPadre.Ni.Ni.nivel, "H");
arbol.NodoPadre.Nd.Ni.Ni.Ni = arbol.CrearNodo(arbol.NodoPadre.Nd.Ni.Ni, arbol.NodoPadre.Nd.Ni.Ni.nivel, "L");
arbol.NodoPadre.Nd.Ni.Ni.Ni.Ni = arbol.CrearNodo(arbol.NodoPadre.Nd.Ni.Ni.Ni, arbol.NodoPadre.Nd.Ni.Ni.Ni.nivel, "M");
arbol.NodoPadre.Nd.Ni.Ni.Ni.Nd = arbol.CrearNodo(arbol.NodoPadre.Nd.Ni.Ni.Ni, arbol.NodoPadre.Nd.Ni.Ni.Ni.nivel, "N");
arbol.NodoPadre.Nd.Ni.Ni.Ni.Nd.Nd = arbol.CrearNodo(arbol.NodoPadre.Nd.Ni.Ni.Ni.Nd, arbol.NodoPadre.Nd.Ni.Ni.Ni.Nd.nivel, "O");
arbol.NodoPadre.Nd.Ni.Ni.Ni.Nd.Nd.Nd = arbol.CrearNodo(arbol.NodoPadre.Nd.Ni.Ni.Ni.Nd.Nd, arbol.NodoPadre.Nd.Ni.Ni.Ni.Nd.Nd.nivel, "P");

arbol.NodoPadre.Nd.Nd =  arbol.CrearNodo(arbol.NodoPadre.Nd, arbol.NodoPadre.Nd.nivel, "F")
arbol.NodoPadre.Nd.Nd.Ni = arbol.CrearNodo(arbol.NodoPadre.Nd.Nd, arbol.NodoPadre.Nd.Nd.nivel, "I");
arbol.NodoPadre.Nd.Nd.Nd = arbol.CrearNodo(arbol.NodoPadre.Nd.Nd, arbol.NodoPadre.Nd.Nd.nivel, "J");

console.log(arbol);

var buscar = [];
var nivel = 2;

buscar = arbol.BusquedaNivel(arbol.NodoPadre, nivel, buscar);
console.log(buscar);

var letra = "O";
var elemento = arbol.BusquedaElemento(arbol.NodoPadre, letra);
console.log(elemento);

var Camino  = arbol.CaminoNodo(elemento);
console.log(Camino);